FROM node:12

WORKDIR /node

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 4545

CMD ["npm", "start"]