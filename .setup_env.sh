#!/bin/bash
echo API_KEY=$API_KEY >> .env
echo NODE_ENV=$NODE_ENV >> .env
echo PORT=$PORT >> .env
echo DBURL_TEST=$DBURL_TEST >> .env
echo DBURL_DEV=$DBURL_DEV >> .env
