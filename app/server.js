const express=require("express");
const app=express();
const unirest=require("./app");
const podcasts=require("./service/crud");
const cors = require("cors");
const PORT=process.env.PORT || 4545;
const db = require("./db/db.config");
var allowlist = ['http://podcast-aws-gitlab.s3-website.eu-west-3.amazonaws.com',
'http://localhost:4200','http://podcast-angular.s3-website.eu-west-3.amazonaws.com', 
'https://angular-podcast.herokuapp.com','http://angular-podcast.herokuapp.com',
'https://podcast-front-gitlab.herokuapp.com','http://podcast-front-gitlab.herokuapp.com',
'http://lespetites-oreilles.com']
var corsOptions = {
 origin: function (origin, callback) {

        if (!origin) {
            return callback(null, true);
        }

        if (allowlist.indexOf(origin) === -1) {

            var msg = 'The CORS policy for this site does not ' +
                'allow access from the specified Origin.';
            return callback(new Error(msg), false);
        }
        return callback(null, true);
    }
};

app.use(cors(corsOptions));
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Credentials", true);
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

 app.get("/",async (req,res)=>{

    await unirest.getAll()
    .then(result=>{res.send(result)});
})

setInterval(podcasts.createPodcasts,(7*24*60*60*1000));


app.get("/podcast",podcasts.createPodcasts); //persister les données dans le mongo
app.get("/podcasts",podcasts.getAllPodcasts);// find tous les podcasts 
app.get("/podcast/:title",podcasts.findPodcastByTitle); // find podcast par le title
app.get("/episodes/:title_epi?",podcasts.findEpisodesByTitle);// find episode(s) par le mot clé du title
app.get("/episode/:episode_id",podcasts.findEpisodeById);
app.get("/allepisodes",podcasts.getAllEpisodes);
app.get("/podcast/episodes/10",podcasts.getNewArrayEpisodes);// afficher toutes les episodes 2/scroll


db.connect()
  .then(() => {
    app.listen(PORT,()=>{
      console.log(`server is running at ${PORT}`);
  });
   
  });

module.exports=app;
