const mongoose=require("mongoose");
var podSchema=mongoose.Schema({

    title:String,
    image:String,
    pod_id:String,
    latest_pub_date:String,
    episodes:[{
        title_epi:String,
        image_epi:String,
        episode_id:String,
        description:String,
        audio:String,
        pub_date:String,
        pub_date_msorigin:Number
    }]
})



var podModel=mongoose.model("Podcast",podSchema);

module.exports=podModel;