const mongoose = require("mongoose");

const dbUrl = "process.env.MONGO_URL || mongodb://localhost:27017/projet4";
const dbUrlIn="mongodb+srv://user_pod:pod@cluster0.tuoim.mongodb.net/projet4?retryWrites=true&w=majority"

function connect() {
    return new Promise((resolve, reject) => {
        if (process.env.NODE_ENV === 'test') {
                    mongoose.connect("mongodb://mongo/projet4", { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false })
                        .then((res, err) => {
                            if (err) return reject(err);

                            else {
                                resolve();
                                console.log("MongoDB test connection successful");
                            }
                        })
        } else {

            mongoose.connect(dbUrlIn, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false })

                .then((res, err) => {
                    if (err) return reject(err);
                    else {
                        resolve();
                        console.log("MongoDB connection successful");
                    }
                })

        }
    })
}

function close() {
    console.log("mongodb disconnect");
    return mongoose.disconnect();
}

module.exports = { connect, close };
