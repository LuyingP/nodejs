const podcastModel = require("../db/podcastShema");
const mongoose = require("mongoose");
const unirest = require("../app");

function convertireTime(ms) {
  var datetime = ms;
  var date = new Date(datetime);
  var options = {
    year: 'numeric', month: 'long', day: 'numeric',
  };

  var result = date.toLocaleDateString('en', options);
  return result;
}

function strip_html_tags(str) {
  if ((str === null) || (str === ''))
    return;
  else
    str = str.toString();
  return str.replace(/<[^>]*>/g, '');
}

const sauvgarde = async (listePodcasts) => {
  try {
    for (let r = 0; r < listePodcasts.length; r++) {
      let listeEpisodes = [];
      for (let index = 0; index < listePodcasts[r].body.episodes.length; index++) {
        let oneEpi =
        {
          title_epi: null,
          image_epi: null,
          episode_id: null,
          description: null,
          audio: null,
          pub_date: null,
          pub_date_msorigin: null

        };
        oneEpi.title_epi = listePodcasts[r].body.episodes[index].title
        oneEpi.image_epi = listePodcasts[r].body.episodes[index].image;
        oneEpi.episode_id = listePodcasts[r].body.episodes[index].id;
        oneEpi.description = strip_html_tags(listePodcasts[r].body.episodes[index].description);
        oneEpi.audio = listePodcasts[r].body.episodes[index].audio;
        oneEpi.pub_date = convertireTime(listePodcasts[r].body.episodes[index].pub_date_ms);
        oneEpi.pub_date_msorigin = listePodcasts[r].body.episodes[index].pub_date_ms;

        listeEpisodes.push(oneEpi);
      }
      podcast = new podcastModel({

        title: listePodcasts[r].body.title.trim(),
        image: listePodcasts[r].body.image,
        pod_id: listePodcasts[r].body.id,
        latest_pub_date: convertireTime(listePodcasts[r].body.latest_pub_date_ms),
        episodes: listeEpisodes

      })
      await podcast.save();
    }

  }

  catch (err) {
    console.log(err.message || "Some error occurred while creating the podcast.")
  };

};

const createPodcasts = async function () {
  try {
    const listePodcasts = await unirest.getAll();
    for (let r = 0; r < listePodcasts.length; r++) {
    };
    await podcastModel.deleteMany({})
    sauvgarde(listePodcasts);
  }
  catch (err) {
    console.log(err.message);

  }
}

const getAllPodcasts= async (req,res)=>{
  try{
    let allPods=await podcastModel.find({});
    res.send(allPods);
  }
catch(err){
  res.send(err.message);

}
}

exports.findPodcastByTitle = (req, res) => {

  if (req.params.title === "Les aventures de Rodolphe et Gala (Histoires pour enfants)") {
    req.params.title = "Les aventures de Rodolphe et Gala";
  }
  const titleSaisi = req.params.title;
  podcastModel.find({ title: new RegExp(titleSaisi, 'i') })
    .then(data => {
      if (!data)
        res.status(404).send({ message: "Not found podcast with title " + title });
      else {
        if (data.length == 0) {
          res.send("no episode(s) found")
        } else {
          res.send(data);
        }
      }

    })
    .catch(err => {
      res
        .status(500)
        .send(err.message);
    });
}

const getListeAllEpisodes = (listPods) => {
  const listAllEpisodes = [];
  for (let i = 0; i < listPods.length; i++) {
    for (let j = 0; j < listPods[i].episodes.length; j++) {
      listAllEpisodes.push(listPods[i].episodes[j]);
    }
  }
  return listAllEpisodes;

}



//pour le view
const getAllEpisodes = async (req, res) => {
  const data = await podcastModel.find({});
  try {
    const listAllEpisodes = await getListeAllEpisodes(data);
    if (!data) {
      res.status(404).send({ message: "404 error " });
    } else {
      res.send(listAllEpisodes);
      return listAllEpisodes;

    }

  }
  catch (err) {
    res
      .status(500)
      .send(err.message);
  };
};

const findEpisodesByTitle = async (req, res) => {
  const title_epi_saisi = req.params.title_epi;
  if (!req.params.title_epi) {
    res.send("input cant be empty");
  }
  else {
    const data = await podcastModel.find({});
    const listAllEpisodes = await getListeAllEpisodes(data);

    let listEpiRecherche = [];
    for (let a = 0; a < listAllEpisodes.length; a++) {
      if (listAllEpisodes[a].title_epi.toLowerCase().includes(title_epi_saisi.toLowerCase())) {
        listEpiRecherche.push(listAllEpisodes[a]);

      }
    }
    if (listEpiRecherche.length != 0) {
      res.send(listEpiRecherche);
    } else {

      res.send("les episodes recherches non trouvees");
    }
  }
}


const findEpisodeById = async (req, res) => {

  const episode_id_saisi = req.params.episode_id;
  if (!req.params.episode_id) {
    res.send("input cant be empty");
  } else {
    const data = await podcastModel.find({});
    const listAllEpisodes = await getListeAllEpisodes(data);
    for (let a = 0; a < listAllEpisodes.length; a++) {
      if (listAllEpisodes[a].episode_id.trim() == episode_id_saisi.trim()) {
        res.send(listAllEpisodes[a]);
        return listAllEpisodes[a];
      }
    }
    res.send("no episode found");

  }

};



const cutArray = function sliceArray(arr, size) {
  var arr2 = [];
  for (var i = 0; i < arr.length; i = i + size) {
    arr2.push(arr.slice(i, i + size));
  }
  return arr2;
}

const getNewArrayEpisodes = async (req, res) => {

  const list = await podcastModel.find({});
  const listAllEpi = getListeAllEpisodes(list);
  const listAllEpisodesSort = listAllEpi.sort((comparer("pub_date_msorigin")));
  const listPart = cutArray(listAllEpisodesSort, 10);
  res.send(listPart);
  return listPart;

}

const comparer = (element) => {
  return function (epi1, epi2) {
    var a = epi1[element];
    var b = epi2[element];
    return b - a;
  }
}




exports.getAllPodcasts = getAllPodcasts;
exports.getAllEpisodes = getAllEpisodes;
exports.findEpisodesByTitle = findEpisodesByTitle;
exports.createPodcasts = createPodcasts;
exports.getListeAllEpisodes = getListeAllEpisodes;
exports.sauvgarde = sauvgarde;
exports.getNewArrayEpisodes = getNewArrayEpisodes;
exports.findEpisodeById = findEpisodeById;



