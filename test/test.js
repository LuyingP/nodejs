
var chai = require('chai');
const expect=require("chai").expect;
const crud=require("../app/service/crud");
const podmodel = require("../app/db/podcastShema");
const connection=require("../app/db/db.config");
const appServer=require("../app/server");
const should=require("chai").should();
const chaiHttp = require('chai-http');
const assert = require('assert');
chai.use(chaiHttp);


//test unitaire
it("test unitaire getListeAllEpisodes",(done)=>{
    
    listPod=[
              {title:"Hello",
              image:"http:hello.jpg",
              pod_id:"12",
              latest_pub_date:"02 sep 2020",
              episodes:[
                {
                  title_epi:"today",
                  image_epi:"http://today.jpg",
                  episode_id:"12/1",
                  description:"ZAA",
                  audio:"http://today.mp3",
                  pub_date:"02 sep 2020",
                  pub_date_msorigin:1599836400000
                },
                {
                  title_epi:"tomorrow",
                  image_epi:"http://tomorrow.jpg",
                  episode_id:"12/2",
                  description:"ZAB",
                  audio:"http://tomorrow.mp3",
                  pub_date:"03 sep 2020",
                  pub_date_msorigin:1589836400000
                }
              ]
              },
              {title:"Color",
              image:"http:color.jpg",
              pod_id:"15",
              latest_pub_date:"05 sep 2020",
              episodes:[
                {
                  title_epi:"red",
                  image_epi:"http://red.jpg",
                  episode_id:"15/1",
                  description:"BAA",
                  audio:"http://color.mp3",
                  pub_date:"05 sep 2020",
                  pub_date_msorigin:1689836400000
                },
                {
                  title_epi:"yellow",
                  image_epi:"http://yellow.jpg",
                  episode_id:"15/2",
                  description:"BAB",
                  audio:"http://yellow.mp3",
                  pub_date:"06 sep 2020",
                  pub_date_msorigin:1699836400000
                }
              ]
              }
            ];

        const listEpis=crud.getListeAllEpisodes(listPod);
        expect(listEpis.length).to.equal(4);
        done();
  })
  
  describe("test http request", function() {
    this.timeout(5000);
    after((done) => {
      connection.close()
        .then(() => done())
        .catch((err) => done(err));
    })


    it("it should not get the pods", function(done) {
      
      chai.request(appServer)
      .get("/podcastss")
      .end(function(err, res) {
          res.should.have.status(404);           
          done();
      });
  });

  
    it('it should not find the episode without input', function(done) {

      this.timeout(5000); 

      chai.request(appServer)
          .get("/episodes/")
          .end(function(err, res) {
              res.should.have.status(200);
              res.text.should.be.eq("input cant be empty");
              done();
        });
    });

    it('it should find the episode', function(done) {
      this.timeout(5000);
      listpod=[];
      const listepi=[
        {
          title_epi:"today",
          image_epi:"http://today.jpg",
          episode_id:"12/1",
          description:"ZAA",
          audio:"http://today.mp3",
          pub_date:"02 sep 2020",
          pub_date_msorigin:1689836400000
        }];
      const podcast=new podmodel({
        title:"Hello",
        image:"http:hello.jpg",
        pod_id:"12",
        latest_pub_date:"02 sep 2020",
        episodes:listepi
      });
      listpod.push(podcast);
      podcast.save(function(err) {
      chai.request(appServer)
          .get("/episodes/" + podcast.episodes[0].title_epi)
          .end(function(err, res) {
              res.should.have.status(200);
              res.body.should.be.a("array");
              res.body[0].should.have.property('pub_date');
              res.body[0].should.have.property('audio');
              res.body[0].should.have.property('description');
              res.body[0].should.have.property('episode_id');
              res.body[0].should.have.property('image_epi');
              res.body[0].should.have.property('title_epi').eql("today");  
              done();
        });
      });
    });
     
    it('it cant find the episode', function(done) {
      this.timeout(5000);
      chai.request(appServer)
          .get("/episodes/yellow")
          .end(function(err, res) {
              res.text.should.be.eq("les episodes recherches non trouvees");  
              done();
        });
     
    });

  });